$(document).ready(function () {
    // get current URL path and assign 'active' class
    var pathname = window.location.pathname;
    $('#menu > li > a[href="' + pathname + '"]').parent().addClass('active');
});

function goBack() {
    window.history.back();
}

$(document).ready(function () {
    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function (event, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }
            var tmppath = URL.createObjectURL(event.target.files[0]);
            $("img").fadeIn("fast").attr('src', URL.createObjectURL(event.target.files[0]));

            $("#disp_tmp_path").html(tmppath);

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function () {
        readURL(this);
    });
});
$(document).on('click', '#close-preview', function () {
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
            $('.image-preview').popover('show');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});

$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class", "close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger: 'manual',
        html: true,
        title: "<strong>Preview</strong>" + $(closebtn)[0].outerHTML,
        content: "There's no image",
        placement: 'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function () {
        $('.image-preview').attr("data-content", "").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse");
    });
    // Create the preview image
    $(".image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");

        }
        reader.readAsDataURL(file);
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $("img").fadeIn("fast").attr('src', URL.createObjectURL(event.target.files[0]));

        $("#disp_tmp_path1").html(tmppath);
    });
});

$(document).ready(function () {
    $("#insert-lender-admin").bootstrapValidator({
        framework: 'bootstrap',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            lenderName: {
                validators: {
                    notEmpty: {
                        message: 'The lender name is required and cannot be empty'
                    }
                }
            },
            lenderProduct: {
                validators: {
                    notEmpty: {
                        message: 'The product is required and cannot be empty'
                    }
                }
            },
            lenderRatesFrom: {
                validators: {
                    notEmpty: {
                        message: 'The APR is required'
                    },
                    numeric: {
                        message: 'APR must be a number'
                    }
                }
            },
            lenderRatesTo: {
                validators: {
                    notEmpty: {
                        message: 'The APR is required'
                    },
                    numeric: {
                        message: 'APR must be a number'
                    }
                }
            },
            representativeAPR: {
                validators: {
                    notEmpty: {
                        message: 'The APR is required'
                    },
                    numeric: {
                        message: 'APR must be a number'
                    }
                }
            },
            loanAmountFrom: {

                validators: {
                    notEmpty: {
                        message: 'Loan amount is required'
                    },
                    numeric: {
                        message: 'Loan amount must be a number'
                    }
                }
            },
            loanAmountTo: {

                validators: {
                    notEmpty: {
                        message: 'Loan amount is required'
                    },
                    numeric: {
                        message: 'Loan amount must be a number'
                    }
                }
            },
            loanTermFrom: {
                validators: {
                    notEmpty: {
                        message: 'The loan term is required and cannot be empty'
                    }
                }
            },
            loanTermTo: {
                validators: {
                    notEmpty: {
                        message: 'The loan term is required and cannot be empty'
                    }
                }
            },
            aboutLender: {
                validators: {
                    notEmpty: {
                        message: 'The loan description is required and cannot be empty'
                    }
                }
            },
            representativeExample: {
                validators: {
                    notEmpty: {
                        message: 'The representative example is required and cannot be empty'
                    }
                }
            },

            lenderLoantype: {
                validators: {
                    notEmpty: {
                        message: 'The loan type is required and cannot be empty'
                    }
                }
            },
            maxTimeToApply: {
                validators: {
                    notEmpty: {
                        message: 'The time to apply is required'
                    },
                    numeric: {
                        message: 'Time must be a number'
                    }
                }
            },
            ageRequirement: {
                validators: {
                    notEmpty: {
                        message: 'The age is required and cannot be empty'
                    },
                    numeric: {
                        message: 'Age must be a number'
                    }
                }
            },
            debitCardDetails: {
                validators: {
                    notEmpty: {
                        message: 'The debit card details required and cannot be empty'
                    }
                }
            },
            fcaRegNumber: {
                validators: {
                    notEmpty: {
                        message: 'The FCA is required and cannot be empty'
                    }
                }
            },
            lenderLogoFile: {
             validators: {
                    notEmpty: {
                        message: 'The logo is required and cannot be empty'
                    },
                    file: {
                        extension: 'jpeg,jpg,png',
                        type: 'image/jpeg,image/png',
                        // maxSize: 2097152,   // 2048 * 1024
                        message: 'The selected file is not valid, the file must be jpeg,jpg,png'
                    }
                }
            },
            carouselBkgImg: {
                validators: {
                    notEmpty: {
                        message: 'The background image for carousel is required'
                    },
                    file: {
                        extension: 'jpeg,jpg,png',
                        type: 'image/jpeg, image/png',
                        // maxSize: 2097152, //2048 * 1024,
                        message: 'The selected file is not valid, the file must be jpeg,jpg,png'
                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {
        
         e.preventDefault();
        });
});



$(document).ready(function () {
    $('#register-form-admin').bootstrapValidator({
        framework: 'bootstrap',
        message: 'This value is invalid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ForeName: {
                validators: {
                    notEmpty: {
                        message: 'The forename is required and cannot be empty'
                    }
                }
            },
            SurName: {
                validators: {
                    notEmpty: {
                        message: 'The surname is required and cannot be empty '
                    }
                }
            },
            Username: {
                validators: {
                    notEmpty: {
                        message: 'The username is required and cannot be empty'
                    }
                }
            },
            Password: {
                validators: {
                    notEmpty: {
                        message: 'the password is required an cannotbe empty'
                    }
                }
            },
            Confirmpassword: {
                validators: {
                    notEmpty: {
                        message: 'The confirmation password is required and cannot be empty'
                    },
                    identical: {
                        field: 'Password',
                        message: 'The confirmation password is not the same with the password'
                    }
                }
            },
            Role: {
                validators: {
                    notEmpty: {
                        message: 'The role is required and cannot be empty'
                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {
        e.preventDefault();
    });
});

$(document).ready(function () {
    $('#insert-student-admin').bootstrapValidator({
        framework: 'bootstrap',
        message: 'This value is invalid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            studentName: {
                validators: {
                    notEmpty: {
                        message: 'The forename is required and cannot be empty'
                    }
                }
            },
            studentSurname: {
                validators: {
                    notEmpty: {
                        message: 'The surname is required and cannot be empty '
                    }
                }
            },
            YearGroup: {
                validators: {
                    notEmpty: {
                        message: 'The year group is required and cannot be empty'
                    }
                }
            }
            
        }
    }).on('success.form.bv', function(e) {
        e.preventDefault();
    });
});

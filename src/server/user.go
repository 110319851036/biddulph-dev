package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"context"

	"encoding/json"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

//Users holdd the data for the users
type Users struct {
	Active            string
	Title             string
	Description       string
	Username          string
	Users             []User
	ShowImpact        []Impacts
	ShowSubject       []Subjects
	ShowInterventions []Interventions
	SHowTeachers      []Teacher
}

// User Hold the data for login register
type User struct {
	ID       int    `json:"id"`
	Forename string `json:"forename"`
	Surname  string `json:"surname"`
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
	IsActive bool   `json:"is_active"`
}

// Auth encrypted the password andd set session for the user
func Auth(users *User, password string, w http.ResponseWriter, r *http.Request) {
	err := bcrypt.CompareHashAndPassword([]byte(users.Password), []byte(password))

	if err != nil {
		print(err.Error())
		http.Redirect(w, r, r.URL.Path, 301)

		return
	}
	setSession(users.Username, w)
	http.Redirect(w, r, "/admin", 302)

}

// Register function create a user
func Register(w http.ResponseWriter, r *http.Request) {

	db := connect()
	defer db.Close()
	if r.Method != "POST" {

		return
	}
	forename := r.FormValue("forename")
	surname := r.FormValue("surname")
	username := r.FormValue("username")
	password := r.FormValue("password")
	role := r.FormValue("role")

	users := QueryUser(username)

	if (User{}) == users {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if err != nil {
			print(err.Error())
		}

		if len(hashedPassword) != 0 && err == nil {
			stmt, err := db.Prepare("INSERT users SET  role = ?, forename = ?, surname = ?, username = ?, password = ?,created_at = NOW(), updated_at = NOW()")

			if err != nil {
				print(err.Error())
				return
			}
			row, err := stmt.Exec(&role, &forename, &surname, &username, &hashedPassword)

			if err != nil {
				print(err.Error())
				return
			}
			id, _ := row.LastInsertId()

			Auth(&User{
				ID:       int(id),
				Role:     string(role),
				Forename: string(forename),
				Surname:  string(surname),
				Username: string(username),
				Password: string(hashedPassword),
			}, password, w, r)

		}
	}
}

//Admin  function render the admin panel when user login
func Admin(w http.ResponseWriter, r *http.Request) {

	context := GetAdminIndex(w, r)
	userName := getUserName(r)
	context.Username = userName
	context.StudentRecords = getAllStudentRecords(w, r)

	if len(userName) != 0 {
		adminRenderView("admin-index.html", w, context)
		return
	}

	http.Redirect(w, r, "/admin/login", 301)
}

// AdminCheck check render the static content for the logged in user
func AdminCheck(next http.Handler, redirect bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username := getUserName(r)
		if len(username) != 0 {
			ctx := context.WithValue(r.Context(), "Username", username)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			if redirect {
				http.Redirect(w, r, "/admin/login", http.StatusTemporaryRedirect)
				return
			}
			http.Error(w, "Forbidden", http.StatusForbidden)
		}
	})
}

var loginTemplate = template.Must(template.New("SignIn.html").Funcs(templateFuncMap).ParseFiles("views/SignIn.html"))

func login(w http.ResponseWriter, r *http.Request) {

	context := GetLogin()
	if r.Method != "POST" {

		err := loginTemplate.Execute(w, context)
		if err != nil {
			println(err.Error())
		}

		return
	}

	username := r.FormValue("username")
	password := r.FormValue("password")

	users := QueryUser(username)
	if users.IsActive == false {
		http.Error(w, "Your account has been deactivated", http.StatusForbidden)
		return
	}

	if users.Role == "pastoral" {
		setSession(users.Username, w)
		http.Redirect(w, r, "/", http.StatusSeeOther)

		return
	}

	Auth(&users, password, w, r)

}

func logout(w http.ResponseWriter, r *http.Request) {
	clearSession(w)
	http.Redirect(w, r, "/admin", 302)
}
func getAllUsers(w http.ResponseWriter, r *http.Request) []User {
	db := connect()
	defer db.Close()

	username := getUserName(r)

	if len(username) != 0 {
		var users []User
		results, err := db.Query("SELECT id, role, forename, surname, username, password, is_active FROM users")

		if err != nil {
			println(err.Error())
		}

		defer results.Close()

		for results.Next() {
			var user User
			err = results.Scan(&user.ID, &user.Role, &user.Forename, &user.Surname, &user.Username, &user.Password, &user.IsActive)
			if err != nil {
				println(err.Error())
			}

			users = append(users, user)

		}
		return users

	}
	return nil
}

func InsertJSONAdminUser(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var user User

	err := decoder.Decode(&user)
	if err != nil {
		println(err.Error())
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		println(err.Error())
	}

	stmt, _ := db.Prepare("INSERT INTO users (role, forename, surname,username, password, is_active, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, NOW(), NOW())")
	res, err := stmt.Exec(user.Role, user.Forename, user.Surname, user.Username, hashedPassword, user.IsActive)

	if err != nil {
		println(err.Error())
	}
	id, err := res.LastInsertId()

	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to get last insert id"})
	}
	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Inserted Post Into the Database", Body: strconv.Itoa(int(id))})

}

//GetJSONSingleUser return json data for the selected user
func GetJSONSingleUser(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)
	userID, err := strconv.Atoi(vars["id"])

	if err != nil {
		print(err.Error())
		fmt.Fprintf(w, "Not a valid id")
	}

	var user User
	err = db.QueryRow("SELECT id, role, forename, surname, username,is_active  FROM users WHERE id = ?", userID).Scan(&user.ID, &user.Role, &user.Forename, &user.Surname, &user.Username, &user.IsActive)

	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select user from database"})
	}
	json.NewEncoder(w).Encode(user)
}

//UpdateUserJSON update the user to the database using json
func UpdateUserJSON(w http.ResponseWriter, r *http.Request) {

	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var user User
	err := decoder.Decode(&user)

	vars := mux.Vars(r)

	userID := vars["id"]
	ID, err := strconv.Atoi(userID)

	if err != nil {
		print(err.Error())
	}

	if len(user.Password) > 0 {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
		if err != nil {
			print(err.Error())
		}
		print(hashedPassword)
		stmt, err := db.Prepare("UPDATE users SET id= ?, role = ?, forename = ?, surname = ?, username = ?, password = ?, updated_at = NOW() WHERE id = ?")
		_, err = stmt.Exec(user.ID, user.Role, user.Forename, user.Surname, user.Username, hashedPassword, ID)
		if err != nil {
			log.Print(err.Error())
			json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to update User in the Database"})
		}

	} else {

		stmt, err := db.Prepare("UPDATE users SET id= ?, role = ?, forename = ?, surname = ?, username = ?, updated_at = NOW() WHERE id = ?")
		_, err = stmt.Exec(user.ID, user.Role, user.Forename, user.Surname, user.Username, ID)
		if err != nil {
			log.Print(err.Error())
			json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to update User in the Database"})
		}
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Update User in the Database"})
}

//EnableDisableUser update the user if its active or not
func EnableDisableUser(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var user User
	err := decoder.Decode(&user)
	if err != nil {
		println(err.Error())
	}

	vars := mux.Vars(r)

	userID := vars["id"]
	ID, err := strconv.Atoi(userID)

	if err != nil {
		println(err.Error())
	}
	stmt, _ := db.Prepare("UPDATE users SET is_active = ? WHERE id = ?")
	_, err = stmt.Exec(user.IsActive, ID)
	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to update user in the database"})
	}
	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Update Lender in the Database"})
}

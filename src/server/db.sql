
CREATE DATABASE biddulph;
use biddulph;

CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    role VARCHAR(120),
    forename VARCHAR(120),
    surname VARCHAR(120),
    username VARCHAR (50),
    password VARCHAR (120),
    created_at DATETIME NULL,
    updated_at DATETIME NULL
);
ALTER TABLE users add COLUMN is_active BOOL AFTER password;
CREATE TABLE students(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    forename VARCHAR(120),
    surname VARCHAR(120),
    year_group VARCHAR(120),
    created_at DATETIME NULL,
    updated_at DATETIME NULL
);
ALTER TABLE students add COLUMN is_active BOOL AFTER year_group;

CREATE TABLE impact(
    id INT not NULL AUTO_INCREMENT PRIMARY KEY,
    impact VARCHAR(500),
    pastoral BOOL,
    teachers BOOL,
    created_at DATETIME NULL,
    updated_at DATETIME NULL
);
CREATE TABLE subjects(
    id INT not NULL AUTO_INCREMENT PRIMARY KEY,
    subjects VARCHAR(500),
    pastoral BOOL,
    teachers BOOL,
    created_at DATETIME NULL,
    updated_at DATETIME NULL
);
CREATE TABLE intervention(
    id INT not NULL AUTO_INCREMENT PRIMARY KEY,
    intervention VARCHAR(500),
    pastoral BOOL,
    teachers BOOL,
    created_at DATETIME NULL,
    updated_at DATETIME NULL
);
CREATE TABLE student_statistics (
    id  INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    student_id INT,
    student_impact VARCHAR(500),
    student_subject VARCHAR(500),
    student_intervention VARCHAR(500),
    student_price DECIMAL(5,2),
    created_at DATETIME,
    updated_at DATETIME

);
ALTER TABLE student_statistics add COLUMN teacher_name VARCHAR(120) AFTER student_price;
ALTER TABLE student_statistics add  FOREIGN KEY (student_id) REFERENCES students(id) ON UPDATE CASCADE ON DELETE CASCADE;
CREATE TABLE biddulph_teachers (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    teachers_name VARCHAR(120),
    created_at DATETIME,
    updated_at DATETIME
);

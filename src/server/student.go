package main

import (
	"time"
)

//Students hond the data for all the students
type Students struct {
	Username       string
	Role           string
	Title          string
	Description    string
	Active         string
	Students       []Student
	StudentRecords []StudentRecord
}

//SingleStudent return the data gor the given student
type SingleStudent struct {
	Username    string
	Title       string
	Description string
	Active      string
	Student     Student
}

//Student hold the data for a single student
type Student struct {
	ID        int    `json:"id"`
	Forename  string `json:"forename"`
	Surname   string `json:"surname"`
	YearGroup string `json:"year_group"`
	IsActive  bool   `json:"is_active"`
}

//StudentRecords holds the data for all the student records in the database
type StudentRecords struct {
	Username       string
	Title          string
	Description    string
	Active         string
	StudentRecords []StudentRecord
}

//StudentRecord holds the data for all the student records in the database
type StudentRecord struct {
	ID                  int       `json:"id"`
	StudentID           int       `json:"student_id"`
	StudentForename     string    `json:"student_forename"`
	StudentSurname      string    `json:"student_surname"`
	StudentImpact       string    `json:"student_impact"`
	StudentSubject      string    `json:"student_subject"`
	StudentIntervention string    `json:"student_intervention"`
	StudentPrice        float32   `json:"student_price"`
	TeacherName         string    `json:"teacher_name"`
	CreatedAt           time.Time `json:"-"`
}

// HTTPResp holds the json custom messages
type HTTPResp struct {
	Status      int    `json:"status"`
	Description string `json:"description"`
	Body        string `json:"body"`
}

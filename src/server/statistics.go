package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
)

type StudentImpacts struct {
	ID                  int     `json:"id"`
	StudentID           int     `json:"student_id"`
	StudentImpact       string  `json:"student_impact"`
	StudentSubject      string  `json:"student_subject"`
	StudentIntervention string  `json:"student_intervention"`
	TeacherName         string  `json:"teacher_name"`
	StudentPrice        float32 `json:"student_price"`
}

func AddStudentJSONImpacts(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var studentImpacts StudentImpacts
	err := decoder.Decode(&studentImpacts)

	if err != nil {
		println(err.Error())
	}

	stmt, _ := db.Prepare("INSERT INTO student_statistics (student_id, student_impact, student_subject,student_intervention,student_price,teacher_name, created_at, updated_at) values (?, ?, ?, ?, ?,?, NOW(), NOW())")
	res, err := stmt.Exec(studentImpacts.StudentID, studentImpacts.StudentImpact, studentImpacts.StudentSubject, studentImpacts.StudentIntervention, studentImpacts.StudentPrice, studentImpacts.TeacherName)

	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to insert impac into database"})
	}

	id, err := res.LastInsertId()
	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to get last insert id"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Inserted Impact Into the Database", Body: strconv.Itoa(int(id))})
}

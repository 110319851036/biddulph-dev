package main

import (
	"database/sql"
	"os"

	"golang.org/x/crypto/bcrypt"

	_ "github.com/go-sql-driver/mysql"

	"fmt"
	"log"
)

var databaseName = "biddulph"
var databasePassword = "SailIn1985!"
var databaseUser = "root"

type Database struct {
	DBName     string `json:"dbname"`
	Host       string `json:"host"`
	DBUser     string `json:"dbuser"`
	DBPassword string `json:"dbpassword"`
	DBPort     string `json:"dbport"`
}

func checkDatabase() error {
	db, err := sql.Open("mysql", ""+databaseUser+":"+databasePassword+"@tcp(localhost:3306)/")

	if err != nil {
		println(err.Error())
		log.Printf("error opening sql database: %v", err)
		return err
	}
	defer db.Close()
	database := Database{}
	database.DBName = databaseName

	_, err = db.Exec("USE " + database.DBName)

	if err != nil {
		println(err.Error())
		log.Printf("error selecting database: %v", err)
		return err
	}
	return err

}
func createDatabase() (string, error) {

	f, err := os.OpenFile("dbcreation.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Printf("error opening file: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)

	db, err := sql.Open("mysql", "root:SailIn1985!@tcp(127.0.0.1:3306)/")

	if err != nil {
		println(err.Error())
		log.Printf("error opening sql database: %v", err)
	}
	defer db.Close()
	database := Database{}
	database.DBName = databaseName

	_, err = db.Exec("CREATE DATABASE if not exists " + database.DBName)

	if err != nil {
		println(err.Error())
		log.Printf("error creating sql database: %v", err)

		return database.DBName, err
	}

	log.Printf("Database %v ctreated", database.DBName)

	_, err = db.Exec("USE " + database.DBName)
	if err != nil {
		println(err.Error())
		log.Printf("error: %v", err)
		return database.DBName, err
	}
	const CTEATETABLEUSERS = `CREATE TABLE if not exists users(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    role VARCHAR(120),
    forename VARCHAR(120),
    surname VARCHAR(120),
    username VARCHAR (50),
    password VARCHAR (120),
	is_active BOOL,
    created_at DATETIME NULL,
    updated_at DATETIME NULL);`

	_, err = db.Exec(CTEATETABLEUSERS)
	if err != nil {
		println(err.Error())
		log.Printf("error creating sql table: %v", err)
		return database.DBName, err
	}

	forename := "Admin"
	surname := "Admin"
	username := "admin"
	password := "admin"
	role := "administrator"
	isActive := 1

	users := QueryUser(username)

	if (User{}) == users {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if err != nil {
			print(err.Error())
			log.Printf("error creating sql table: %v", err)
			return database.DBName, err
		}

		if len(hashedPassword) != 0 && err == nil {
			stmt, err := db.Prepare("INSERT users SET  role = ?, forename = ?, surname = ?, username = ?, password = ?,is_active = ?,created_at = NOW(), updated_at = NOW()")

			if err != nil {
				print(err.Error())
				log.Printf("error creating sql table: %v", err)

				return database.DBName, err
			}
			row, err := stmt.Exec(&role, &forename, &surname, &username, &hashedPassword, &isActive)

			if err != nil {
				print(err.Error())
				log.Printf("error creating sql table: %v", err)
				return database.DBName, err
			}
			id, err := row.LastInsertId()
			if err != nil {
				print(err.Error())
				log.Printf("error creating sql table:%v %v", err, id)
				return database.DBName, err
			}
		}
	}

	const CREATETABLESTUDENTS = `CREATE TABLE if not exists students(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    forename VARCHAR(120),
    surname VARCHAR(120),
    year_group VARCHAR(120),
    is_active BOOL,
    created_at DATETIME NULL,
    updated_at DATETIME NULL);`

	_, err = db.Exec(CREATETABLESTUDENTS)
	if err != nil {
		println(err.Error())
		log.Printf("error creating sql table: %v", err)
		return database.DBName, err
	}
	const CREATETABLEIMPACT = `CREATE TABLE if not exists impact(
    id INT not NULL AUTO_INCREMENT PRIMARY KEY,
    impact VARCHAR(500),
    pastoral BOOL,
    teachers BOOL,
    created_at DATETIME NULL,
    updated_at DATETIME NULL);`

	_, err = db.Exec(CREATETABLEIMPACT)
	if err != nil {
		println(err.Error())
		log.Printf("error creating sql table: %v", err)
		return database.DBName, err
	}

	const INSERTIMPACTS = `INSERT INTO impact (impact,pastoral,teachers,created_at,updated_at) VALUES 
	('Attendance Increased',1,1,NOW(),NOW()),
	('Reduction in Behavioural Incidents',1,1,NOW(),NOW()),
	('Increased Participation and Engagement',1,1,NOW(),NOW()),
	('+1 Grade Increase',0,1,NOW(),NOW()),
	('+2 Grade Increase',0,1,NOW(),NOW()),
	('= Grade Maintained',0,1,NOW(),NOW()),
	('-1 Grade Decrease',0,1,NOW(),NOW()),
	('-2 Grade Decrease',0,1,NOW(),NOW()),
	('Improved Attitude to Learning',0,1,NOW(),NOW()),
	('Progress 8 Positive',1,1,NOW(),NOW()),
	('Reduction in Risk Taking Behaviour',1,0,NOW(),NOW()),
	('No Longer at Risk of Permanent Exclusion',1,0,NOW(),NOW());`
	_, err = db.Exec(INSERTIMPACTS)
	if err != nil {
		println(err.Error())
		log.Printf("error Inserting values into Impacts: %v", err.Error())
		return database.DBName, err
	}
	const CREATETABLESUBJECTS = `CREATE TABLE if not exists subjects(
    id INT not NULL AUTO_INCREMENT PRIMARY KEY,
    subjects VARCHAR(500),
    pastoral BOOL,
    teachers BOOL,
    created_at DATETIME NULL,
    updated_at DATETIME NULL);`

	_, err = db.Exec(CREATETABLESUBJECTS)
	if err != nil {
		println(err.Error())
		log.Printf("error creating sql table: %v", err)
		return database.DBName, err
	}
	const CREATETABLEINTERVANTION = `CREATE TABLE if not exists intervention(
    id INT not NULL AUTO_INCREMENT PRIMARY KEY,
    intervention VARCHAR(500),
    pastoral BOOL,
    teachers BOOL,
    created_at DATETIME NULL,
    updated_at DATETIME NULL);`

	_, err = db.Exec(CREATETABLEINTERVANTION)

	if err != nil {
		println(err.Error())
		log.Printf("error creating sql table: %v", err)
		return database.DBName, err
	}
	const INSERTINTERVENTIONS = `INSERT INTO intervention (intervention,pastoral,teachers,created_at,updated_at) VALUES
	('1:1 support',0,1,NOW(),NOW()),
	('1:1 tutorial',0,1,NOW(),NOW()),
	('SAM Learning',1,1,NOW(),NOW()),
	('Revision Guides',0,1,NOW(),NOW()),
	('How to revise sessions',1,1,NOW(),NOW()),
	('Exam Practice Referral',0,1,NOW(),NOW()),
	('Phone call home',0,1,NOW(),NOW()),
	('Progress Meeting with parent',0,1,NOW(),NOW()),
	('Progress and Achievement Report',0,1,NOW(),NOW()),
	('Revision Session',0,1,NOW(),NOW()),
	('PIE Club',0,1,NOW(),NOW()),
	('Maths for Science Club',0,1,NOW(),NOW()),
	('Specific Differentiated Resources',0,1,NOW(),NOW()),
	('Holiday Revision Session',1,1,NOW(),NOW()),
	('Equipment',0,1,NOW(),NOW()),
	('Set Change to provide challenge',0,1,NOW(),NOW()),
	('1:1 additional teaching',1,0,NOW(),NOW()),
	('Pastoral Support',1,0,NOW(),NOW()),
	('Study Cafe',1,0,NOW(),NOW()),
	('Saturday Maths',1,0,NOW(),NOW()),
	('Careers Interview',1,0,NOW(),NOW()),
	('College Placement',1,0,NOW(),NOW()),
	('Referral to School Nurse',1,0,NOW(),NOW()),
	('Referral to Drugs and Alcohol',1,0,NOW(),NOW()),
	('Referral to Mentor',1,0,NOW(),NOW()),
	('Referral to School Counsellor',1,0,NOW(),NOW()),
	('Careers Experience Visit',1,0,NOW(),NOW()),
	('University Experience Visit',1,0,NOW(),NOW()),
	('LG Mentoring',1,0,NOW(),NOW()),
	('LG Progress and Achievement Report',1,0,NOW(),NOW()),
	('Progress Tutor Mentoring',1,0,NOW(),NOW());`

	_, err = db.Exec(INSERTINTERVENTIONS)
	if err != nil {
		println(err.Error())
		log.Printf("error inserting values into intervention table: %v", err.Error())
		return database.DBName, err
	}
	const CREATETABLESTUDENTSTATISTICS = `CREATE TABLE if not exists student_statistics (
    id  INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    student_id INT,
    student_impact VARCHAR(500),
    student_subject VARCHAR(500),
    student_intervention VARCHAR(500),
    student_price DECIMAL(5,2),
    teacher_name VARCHAR(120),
    created_at DATETIME,
    updated_at DATETIME,
    FOREIGN KEY (student_id) REFERENCES students(id) ON UPDATE CASCADE ON DELETE CASCADE);`
	_, err = db.Exec(CREATETABLESTUDENTSTATISTICS)
	if err != nil {
		println(err.Error())
		log.Printf("error creating sql table: %v", err)
		return database.DBName, err
	}
	const CREATETABLETEACHERS = `CREATE TABLE if not exists biddulph_teachers (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    teachers_name VARCHAR(120),
    created_at DATETIME,
    updated_at DATETIME);`

	_, err = db.Exec(CREATETABLETEACHERS)
	if err != nil {
		println(err.Error())
		log.Printf("error creating sql table: %v", err)
		return database.DBName, err
	}
	const CREATEVIEWSTATISTICSVIEW = "CREATE OR REPLACE VIEW  `statistics_view` AS SELECT  `students`.`forename` AS `forename`, `students`.`surname` AS `surname`, `student_statistics`.`student_id` AS `student_id`, `student_statistics`.`id` AS `id`, `student_statistics`.`student_impact` AS `student_impact`, `student_statistics`.`student_intervention` AS `student_intervention`, `student_statistics`.`student_subject` AS `student_subject`, `student_statistics`.`student_price` AS `student_price`, `student_statistics`.`teacher_name` AS `teacher_name`, `student_statistics`.`created_at` AS `created_at` FROM (`students` LEFT JOIN `student_statistics` ON ((`students`.`id` = `student_statistics`.`student_id`)))"
	_, err = db.Exec(CREATEVIEWSTATISTICSVIEW)
	if err != nil {
		println(err.Error())
		log.Printf("error creating sql table: %v", err)
		return database.DBName, err
	}
	return database.DBName, err
}

func connect() *sql.DB {

	// db, err := sql.Open("mysql", os.Getenv("MYSQL_USER")+":"+os.Getenv("MYSQL_PASS")+"@("+os.Getenv("MYSQL_HOST")+")/"+os.Getenv("MYSQL_DB")+"?parseTime=true")
	db, err := sql.Open("mysql", ""+databaseUser+":"+databasePassword+"@tcp(localhost:3306)/"+databaseName+"?parseTime=true")

	if err != nil {
		log.Fatal("Could not connect to database")
	}

	err = db.Ping()

	if err != nil {
		log.Fatal("Could not connect to database" + err.Error())
	}

	fmt.Println("You connected to the database")

	return db
}

//QueryUser resturn the given user details
func QueryUser(userName string) User {
	db := connect()
	defer db.Close()

	var users = User{}
	db.QueryRow("SELECT id, role,username,password,is_active FROM users where username = ?", userName).Scan(&users.ID, &users.Role, &users.Username, &users.Password, &users.IsActive)
	return users
}

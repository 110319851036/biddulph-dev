package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func getAllStudents(w http.ResponseWriter, r *http.Request) []Student {
	db := connect()
	defer db.Close()

	username := getUserName(r)

	if len(username) != 0 {
		var students []Student
		result, err := db.Query("SELECT id, forename, surname, year_group, is_active FROM students")
		if err != nil {
			print(err.Error())
		}
		defer result.Close()
		for result.Next() {
			var student Student
			err = result.Scan(&student.ID, &student.Forename, &student.Surname, &student.YearGroup, &student.IsActive)
			if err != nil {
				println(err.Error())
			}
			students = append(students, student)
		}
		return students
	}
	return nil
}
func GetJSONAllStudentsNames(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	var students []Student
	results, err := db.Query("SELECT id, forename, surname, year_group, is_active FROM students")

	for results.Next() {
		var student Student
		err = results.Scan(&student.ID, &student.Forename, &student.Surname, &student.YearGroup, &student.IsActive)
		if err != nil {
			json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select all from lenders"})
		}
		students = append(students, student)
	}

	json.NewEncoder(w).Encode(students)

}
func InsertJSONStudent(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)
	var student Student
	err := decoder.Decode(&student)

	if err != nil {
		println(err.Error())
	}

	stmt, _ := db.Prepare("INSERT INTO students (forename, surname, year_group, is_active, created_at, updated_at) values (?,?,?,?, NOW(), NOW()) ")
	res, err := stmt.Exec(student.Forename, student.Surname, student.YearGroup, student.IsActive)
	if err != nil {
		println(err.Error()) // proper error handling instead of panic in your app
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to insert student into database"})
	}

	id, err := res.LastInsertId()
	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to get last insert id"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Inserted Student Into the Database", Body: strconv.Itoa(int(id))})

}
func GetJSONSingleStudent(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)
	studentID, err := strconv.Atoi(vars["id"])

	if err != nil {
		print(err.Error())
		fmt.Fprintf(w, "Not a valid id")
	}

	var student Student
	err = db.QueryRow("SELECT id, forename, surname, year_group ,is_active  FROM students WHERE id = ?", studentID).Scan(&student.ID, &student.Forename, &student.Surname, &student.YearGroup, &student.IsActive)

	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select user from database"})
	}
	json.NewEncoder(w).Encode(student)
}
func UpdateJSONStudent(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var student Student
	err := decoder.Decode(&student)

	vars := mux.Vars(r)
	studentID := vars["id"]
	ID, err := strconv.Atoi(studentID)

	if err != nil {
		println(err.Error())
	}

	stmt, err := db.Prepare("UPDATE students SET id= ?, forename = ?, surname = ?, year_group = ?, is_active = ?, updated_at = NOW() where id = ?")
	_, err = stmt.Exec(student.ID, student.Forename, student.Surname, student.YearGroup, student.IsActive, ID)
	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to update Student in the Database"})
	}
	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Update Student in the Database"})
}
func DeleteJSONStudent(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)

	studentID := vars["id"]
	ID, _ := strconv.Atoi(studentID)

	stmt, err := db.Prepare("DELETE FROM students where id = ?")

	if err != nil {
		println(err.Error())
	}

	_, err = stmt.Exec(ID)
	if err != nil {
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to delete student from database"})
	}
	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully deleted student from database "})
}

package main

import "net/http"

// GetLender return to template details for each lender

// Get404 return error if page doesnt exists
func Get404() Students {
	var result Students
	result.Active = "404"
	result.Title = " High-School | Page not found"
	return result
}

// GetHome return page details for home page
func GetHome(w http.ResponseWriter, r *http.Request) Students {
	var result Students
	result.Active = "home"
	result.Title = " High School  "
	result.Description = " High School"

	return result
}

// GetLogin holds the data  for login page
func GetLogin() Students {
	var result Students
	result.Active = "login"
	result.Title = " High-School  | Log In"
	result.Description = " High-School  | Log In"
	return result
}

// GetRegister holds the data  for register page
func GetRegister() Students {
	var result Students
	result.Active = "register"
	result.Title = " High School  "
	result.Description = " High School"
	return result
}

// GetAdminIndex hold the data for admin panel page
func GetAdminIndex(w http.ResponseWriter, r *http.Request) Students {

	var result Students
	result.Active = "register"
	result.Title = " High School  "
	result.Description = " High School"

	return result
}

//GetAdminAllStudents render the students page with data
func GetAdminAllStudents(w http.ResponseWriter, r *http.Request) Students {
	var result Students
	result.Active = "register"
	result.Title = " High School  "
	result.Description = " High School"
	result.Students = getAllStudents(w, r)

	return result

}

//GetInsertStudent render the students page with data
func GetInsertStudent() Students {
	var result Students
	result.Active = "register"
	result.Title = " High School  "
	result.Description = " High School"

	return result
}
func GetAdminUsers(w http.ResponseWriter, r *http.Request) Users {
	var result Users
	result.Active = "admin users"
	result.Title = " High School  "
	result.Description = " High School"
	result.Users = getAllUsers(w, r)
	return result
}
func GetAdminHelpers(w http.ResponseWriter, r *http.Request) Users {
	var result Users
	result.Active = "admin users"
	result.Title = " High School  "
	result.Description = " High School"
	result.Users = getAllUsers(w, r)
	return result
}

func EditRecords() StudentRecords {
	var result StudentRecords
	result.Active = "admin records"
	result.Title = " High School  "
	result.Description = " High School"
	return result
}

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type Impacts struct {
	ID       int    `json:"id"`
	Impact   string `json:"impact"`
	Pastoral bool   `json:"pastoral"`
	Teachers bool   `json:"teachers"`
}
type Subjects struct {
	ID       int    `json:"id"`
	Subject  string `json:"subject"`
	Pastoral bool   `json:"pastoral"`
	Teachers bool   `json:"teachers"`
}
type Interventions struct {
	ID           int    `json:"id"`
	Intervention string `json:"intervention"`
	Pastoral     bool   `json:"pastoral"`
	Teachers     bool   `json:"teachers"`
}
type Teacher struct {
	ID          int    `json:"id"`
	TeacherName string `json:"teacher_name"`
}

func FormatDateTime(dateTime time.Time) string {

	return dateTime.Format("3:04 PM 01/02/2006")
}

//Intervention table

func getAllInterventions(w http.ResponseWriter, r *http.Request) []Interventions {
	db := connect()
	defer db.Close()

	username := getUserName(r)

	if len(username) != 0 {
		var interventions []Interventions
		result, err := db.Query("SELECT id, intervention, pastoral, teachers FROM intervention")
		if err != nil {
			print(err.Error())
		}
		defer result.Close()
		for result.Next() {
			var intervention Interventions
			err = result.Scan(&intervention.ID, &intervention.Intervention, &intervention.Pastoral, &intervention.Teachers)
			if err != nil {
				println(err.Error())
			}
			interventions = append(interventions, intervention)

		}
		return interventions
	}
	return nil
}
func GetJSONAllInterventions(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	var interventions []Interventions
	results, err := db.Query("SELECT id, intervention,pastoral, teachers FROM intervention")

	for results.Next() {
		var intervention Interventions
		err = results.Scan(&intervention.ID, &intervention.Intervention, &intervention.Pastoral, &intervention.Teachers)
		if err != nil {
			json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select all from lenders"})
		}
		interventions = append(interventions, intervention)
	}

	json.NewEncoder(w).Encode(interventions)

}
func InsertJSONIntervention(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)
	var intervention Interventions
	err := decoder.Decode(&intervention)

	if err != nil {
		println(err.Error())
	}

	stmt, _ := db.Prepare("INSERT INTO intervention (intervention, pastoral, teachers, created_at, updated_at) values (?,?,?, NOW(), NOW()) ")
	res, err := stmt.Exec(intervention.Intervention, intervention.Pastoral, intervention.Teachers)
	if err != nil {
		println(err.Error()) // proper error handling instead of panic in your app
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to insert intervention into database"})
	}

	id, err := res.LastInsertId()
	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to get last insert id"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Inserted intervention Into the Database", Body: strconv.Itoa(int(id))})

}
func GetJSONSingleIntervention(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)
	interventionID, err := strconv.Atoi(vars["id"])

	if err != nil {
		print(err.Error())
		fmt.Fprintf(w, "Not a valid id")
	}

	var intervention Interventions
	err = db.QueryRow("SELECT id, intervention, pastoral, teachers   FROM intervention WHERE id = ?", interventionID).Scan(&intervention.ID, &intervention.Intervention, &intervention.Pastoral, &intervention.Teachers)

	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select intervention from database"})
	}
	json.NewEncoder(w).Encode(intervention)
}
func UpdateInterventionJSON(w http.ResponseWriter, r *http.Request) {

	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var intervention Interventions
	err := decoder.Decode(&intervention)

	vars := mux.Vars(r)

	interventionID := vars["id"]

	ID, err := strconv.Atoi(interventionID)
	println(ID)

	if err != nil {
		print(err.Error())
	}

	stmt, err := db.Prepare("UPDATE intervention SET id= ?, intervention = ?, pastoral = ?, teachers = ?, updated_at = NOW() WHERE id = ?")
	_, err = stmt.Exec(intervention.ID, intervention.Intervention, intervention.Pastoral, intervention.Teachers, ID)
	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to update intervention in the Database"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Update intervention in the Database"})
}
func DeleteJSONIntervention(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)

	interventiontID := vars["id"]
	ID, _ := strconv.Atoi(interventiontID)

	stmt, err := db.Prepare("DELETE FROM intervention where id = ?")

	if err != nil {
		println(err.Error())
	}

	_, err = stmt.Exec(ID)
	if err != nil {
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to delete intervention from database"})
	}
	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully deleted intervention from database "})
}

//Subject table

func getAllSubjects(w http.ResponseWriter, r *http.Request) []Subjects {
	db := connect()
	defer db.Close()

	username := getUserName(r)

	if len(username) != 0 {
		var subjects []Subjects
		result, err := db.Query("SELECT id, subjects, pastoral, teachers FROM subjects")
		if err != nil {
			print(err.Error())
		}
		defer result.Close()
		for result.Next() {
			var subject Subjects
			err = result.Scan(&subject.ID, &subject.Subject, &subject.Pastoral, &subject.Teachers)
			if err != nil {
				println(err.Error())
			}
			subjects = append(subjects, subject)

		}
		return subjects
	}
	return nil
}
func GetJSONAllSubjects(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	var subjects []Subjects
	results, err := db.Query("SELECT id, subjects,pastoral,teachers FROM subjects")

	for results.Next() {
		var subject Subjects
		err = results.Scan(&subject.ID, &subject.Subject, &subject.Pastoral, &subject.Teachers)
		if err != nil {
			json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select all from lenders"})
		}
		subjects = append(subjects, subject)
	}

	json.NewEncoder(w).Encode(subjects)

}
func InsertJSONSubjects(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)
	var subject Subjects
	err := decoder.Decode(&subject)

	if err != nil {
		println(err.Error())
	}

	stmt, _ := db.Prepare("INSERT INTO subjects (subjects, pastoral, teachers, created_at, updated_at) values (?,?,?, NOW(), NOW()) ")
	res, err := stmt.Exec(subject.Subject, subject.Pastoral, subject.Teachers)
	if err != nil {
		println(err.Error()) // proper error handling instead of panic in your app
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to insert subject into database"})
	}

	id, err := res.LastInsertId()
	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to get last insert id"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Inserted subject Into the Database", Body: strconv.Itoa(int(id))})

}
func GetJSONSingleSubject(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)
	subjectID, err := strconv.Atoi(vars["id"])

	if err != nil {
		print(err.Error())
		fmt.Fprintf(w, "Not a valid id")
	}

	var subject Subjects
	err = db.QueryRow("SELECT id, subjects, pastoral, teachers   FROM subjects WHERE id = ?", subjectID).Scan(&subject.ID, &subject.Subject, &subject.Pastoral, &subject.Teachers)

	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select Subject from database"})
	}
	json.NewEncoder(w).Encode(subject)
}
func UpdateSubjectJSON(w http.ResponseWriter, r *http.Request) {

	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var subject Subjects
	err := decoder.Decode(&subject)

	vars := mux.Vars(r)

	subjectID := vars["id"]

	ID, err := strconv.Atoi(subjectID)
	println(ID)

	if err != nil {
		print(err.Error())
	}

	stmt, err := db.Prepare("UPDATE subjects SET id= ?, subjects = ?, pastoral = ?, teachers = ?, updated_at = NOW() WHERE id = ?")
	_, err = stmt.Exec(subject.ID, subject.Subject, subject.Pastoral, subject.Teachers, ID)
	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to update Subject in the Database"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Update Subject in the Database"})
}
func DeleteJSONSubject(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)

	subjectID := vars["id"]
	ID, _ := strconv.Atoi(subjectID)

	stmt, err := db.Prepare("DELETE FROM subjects where id = ?")

	if err != nil {
		println(err.Error())
	}

	_, err = stmt.Exec(ID)
	if err != nil {
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to delete subject from database"})
	}
	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully deleted subject from database "})
}

// Impact table

func getAllImpacts(w http.ResponseWriter, r *http.Request) []Impacts {
	db := connect()
	defer db.Close()

	username := getUserName(r)

	if len(username) != 0 {
		var impacts []Impacts
		result, err := db.Query("SELECT id, impact, pastoral, teachers FROM impact")
		if err != nil {
			print(err.Error())
		}
		defer result.Close()
		for result.Next() {
			var impact Impacts
			err = result.Scan(&impact.ID, &impact.Impact, &impact.Pastoral, &impact.Teachers)
			if err != nil {
				println(err.Error())
			}
			impacts = append(impacts, impact)

		}
		return impacts
	}
	return nil
}

func GetJSONAllImpacts(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	var impacts []Impacts
	results, err := db.Query("SELECT id, impact,pastoral, teachers FROM impact")

	for results.Next() {
		var impact Impacts
		err = results.Scan(&impact.ID, &impact.Impact, &impact.Pastoral, &impact.Teachers)
		if err != nil {
			json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select all from lenders"})
		}
		impacts = append(impacts, impact)
	}

	json.NewEncoder(w).Encode(impacts)

}
func InsertJSONImpact(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)
	var impact Impacts
	err := decoder.Decode(&impact)

	if err != nil {
		println(err.Error())
	}

	stmt, _ := db.Prepare("INSERT INTO impact (impact, pastoral, teachers, created_at, updated_at) values (?,?,?, NOW(), NOW()) ")
	res, err := stmt.Exec(impact.Impact, impact.Pastoral, impact.Teachers)
	if err != nil {
		println(err.Error()) // proper error handling instead of panic in your app
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to insert impact into database"})
	}

	id, err := res.LastInsertId()
	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to get last insert id"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Inserted impact Into the Database", Body: strconv.Itoa(int(id))})

}

func GetJSONSingleImpact(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)
	impactID, err := strconv.Atoi(vars["id"])

	if err != nil {
		print(err.Error())
		fmt.Fprintf(w, "Not a valid id")
	}

	var impact Impacts
	err = db.QueryRow("SELECT id, impact, pastoral, teachers   FROM impact WHERE id = ?", impactID).Scan(&impact.ID, &impact.Impact, &impact.Pastoral, &impact.Teachers)

	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select Impact from database"})
	}
	json.NewEncoder(w).Encode(impact)
}
func UpdateImpactJSON(w http.ResponseWriter, r *http.Request) {

	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var impact Impacts
	err := decoder.Decode(&impact)

	vars := mux.Vars(r)

	impactID := vars["id"]
	println(impactID)
	ID, err := strconv.Atoi(impactID)
	println(ID)

	if err != nil {
		print(err.Error())
	}

	stmt, err := db.Prepare("UPDATE impact SET id= ?, impact = ?, pastoral = ?, teachers = ?, updated_at = NOW() WHERE id = ?")
	_, err = stmt.Exec(impact.ID, impact.Impact, impact.Pastoral, impact.Teachers, ID)
	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to update Impact in the Database"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Update Impact in the Database"})
}
func DeleteJSONImpact(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)

	impactID := vars["id"]
	ID, _ := strconv.Atoi(impactID)

	stmt, err := db.Prepare("DELETE FROM impact where id = ?")

	if err != nil {
		println(err.Error())
	}

	_, err = stmt.Exec(ID)
	if err != nil {
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to delete impact from database"})
	}
	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully deleted impact from database "})
}

func getAllStudentRecords(w http.ResponseWriter, r *http.Request) []StudentRecord {
	db := connect()
	defer db.Close()

	username := getUserName(r)

	if len(username) != 0 {
		var studentRecords []StudentRecord

		result, err := db.Query("SELECT students.forename,students.surname, student_statistics.student_id,student_statistics.id, student_statistics.student_impact,student_statistics.student_intervention, student_statistics.student_subject,student_statistics.student_price,student_statistics.teacher_name,student_statistics.created_at FROM students LEFT JOIN student_statistics ON students.id = student_statistics.student_id ")

		if err != nil {
			println(err.Error())
		}
		defer result.Close()

		for result.Next() {
			var studentRecord StudentRecord

			err = result.Scan(&studentRecord.StudentForename, &studentRecord.StudentSurname, &studentRecord.StudentID, &studentRecord.ID, &studentRecord.StudentImpact, &studentRecord.StudentIntervention, &studentRecord.StudentSubject, &studentRecord.StudentPrice, &studentRecord.TeacherName, &studentRecord.CreatedAt)

			if err != nil {
				println(err.Error())
			}
			studentRecords = append(studentRecords, studentRecord)
		}
		// fmt.Println(studentRecords)
		// res2B, _ := json.Marshal(studentRecords)
		// fmt.Println("\n", string(res2B))
		return studentRecords
	}
	return nil
}

func GetJSONSingleStudentRecord(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])

	if err != nil {
		print(err.Error())
		fmt.Fprintf(w, "Not a valid id")
	}

	var record StudentRecord

	err = db.QueryRow("SELECT id, student_id, forename,surname, student_impact,student_intervention, student_subject, student_price,teacher_name FROM statistics_view where id = ?", ID).Scan(&record.ID, &record.StudentID, &record.StudentForename, &record.StudentSurname, &record.StudentImpact, &record.StudentIntervention, &record.StudentSubject, &record.StudentPrice, &record.TeacherName)
	// err = db.QueryRow("SELECT students.forename,students.surname, student_statistics.student_id,student_statistics.id, student_statistics.student_impact,student_statistics.student_intervention, student_statistics.student_subject,student_statistics.student_price FROM students LEFT JOIN student_statistics ON students.id = student_statistics.student_id where student_id = ?;", ID).Scan(&record.StudentForename, &record.StudentSurname, &record.StudentID, &record.ID, &record.StudentImpact, &record.StudentIntervention, &record.StudentSubject, &record.StudentPrice)

	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select student from database"})
	}
	json.NewEncoder(w).Encode(record)
}
func UpdateStudentRecordJSON(w http.ResponseWriter, r *http.Request) {

	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var record StudentRecord
	err := decoder.Decode(&record)

	vars := mux.Vars(r)

	SID := vars["id"]

	ID, err := strconv.Atoi(SID)
	println(ID)

	if err != nil {
		print(err.Error())
	}

	stmt, err := db.Prepare("UPDATE student_statistics SET id= ?, student_id= ?, student_impact = ?, student_intervention = ?, student_subject = ?, student_price= ?,teacher_name= ?, updated_at = NOW() WHERE id = ?")
	_, err = stmt.Exec(record.ID, record.StudentID, record.StudentImpact, record.StudentIntervention, record.StudentSubject, record.StudentPrice, record.TeacherName, ID)
	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to update student record in the Database"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Update student record in the Database"})
}
func DeleteJSONStudentRecord(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)

	impactID := vars["id"]
	ID, _ := strconv.Atoi(impactID)

	stmt, err := db.Prepare("DELETE FROM student_statistics where id = ?")

	if err != nil {
		println(err.Error())
	}

	_, err = stmt.Exec(ID)
	if err != nil {
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to delete student record from database"})
	}
	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully deleted student record from database "})
}

// Teachers

func getAllTeachers(w http.ResponseWriter, r *http.Request) []Teacher {
	db := connect()
	defer db.Close()

	username := getUserName(r)

	if len(username) != 0 {
		var teachers []Teacher
		result, err := db.Query("SELECT id, teachers_name  FROM biddulph_teachers")
		if err != nil {
			print(err.Error())
		}
		defer result.Close()
		for result.Next() {
			var teacher Teacher
			err = result.Scan(&teacher.ID, &teacher.TeacherName)
			if err != nil {
				println(err.Error())
			}
			teachers = append(teachers, teacher)

		}
		return teachers
	}
	return nil
}

func GetJSONAllTeachers(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	var teachers []Teacher
	results, err := db.Query("SELECT id, teachers_name FROM biddulph_teachers")

	for results.Next() {
		var teacher Teacher
		err = results.Scan(&teacher.ID, &teacher.TeacherName)
		if err != nil {
			json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select all from teachers table"})
		}
		teachers = append(teachers, teacher)
	}

	json.NewEncoder(w).Encode(teachers)

}
func InsertJSONTeacher(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)
	var teacher Teacher
	err := decoder.Decode(&teacher)

	if err != nil {
		println(err.Error())
	}

	stmt, _ := db.Prepare("INSERT INTO biddulph_teachers (teachers_name,  created_at, updated_at) values (?, NOW(), NOW()) ")
	res, err := stmt.Exec(teacher.TeacherName)
	if err != nil {
		println(err.Error()) // proper error handling instead of panic in your app
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to insert teacher into database"})
	}

	id, err := res.LastInsertId()
	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to get last insert id"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Inserted teacher Into the Database", Body: strconv.Itoa(int(id))})

}

func GetJSONSingleTeacher(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)
	teacherID, err := strconv.Atoi(vars["id"])

	if err != nil {
		print(err.Error())
		fmt.Fprintf(w, "Not a valid id")
	}

	var teacher Teacher
	err = db.QueryRow("SELECT id, teachers_name  FROM biddulph_teachers WHERE id = ?", teacherID).Scan(&teacher.ID, &teacher.TeacherName)

	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to select Teacher from database"})
	}
	json.NewEncoder(w).Encode(teacher)
}
func UpdateTeacherJSON(w http.ResponseWriter, r *http.Request) {

	db := connect()
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var teacher Teacher
	err := decoder.Decode(&teacher)

	vars := mux.Vars(r)

	teacherID := vars["id"]
	ID, err := strconv.Atoi(teacherID)

	if err != nil {
		print(err.Error())
	}

	stmt, err := db.Prepare("UPDATE biddulph_teachers SET id= ?, teachers_name = ?, updated_at = NOW() WHERE id = ?")
	_, err = stmt.Exec(teacher.ID, teacher.TeacherName, ID)
	if err != nil {
		log.Print(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to update Teacher in the Database"})
	}

	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Update Teacher in the Database"})
}
func DeleteJSONTeacher(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	vars := mux.Vars(r)

	teacherID := vars["id"]
	ID, _ := strconv.Atoi(teacherID)

	stmt, err := db.Prepare("DELETE FROM biddulph_teachers where id = ?")

	if err != nil {
		println(err.Error())
	}

	_, err = stmt.Exec(ID)
	if err != nil {
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Failed to delete teacher from database"})
	}
	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully deleted teacher from database "})
}

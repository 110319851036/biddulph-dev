package main

import (
	"net/http"

	"github.com/gorilla/securecookie"
)

var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

func getUserName(r *http.Request) (userName string) {
	if cookie, err := r.Cookie("biddulph"); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode("biddulph", cookie.Value, &cookieValue); err == nil {
			userName = cookieValue["name"]
		}
	}
	return userName
}

func setSession(userName string, w http.ResponseWriter) {
	value := map[string]string{
		"name": userName,
	}
	if encoded, err := cookieHandler.Encode("biddulph", value); err == nil {
		cookie := &http.Cookie{
			Name:  "biddulph",
			Value: encoded,
			Path:  "/",
		}
		http.SetCookie(w, cookie)
	}
}

func clearSession(w http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "biddulph",
		Value:  "",
		Path:   "/",
		MaxAge: 0,
	}
	http.SetCookie(w, cookie)
}

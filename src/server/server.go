package main

import (
	"html/template"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var templateCache = map[string]*template.Template{}

var templateFuncMap = template.FuncMap{
	"safehtml": safeHTML,
}

func renderView(viewName string, w http.ResponseWriter, data interface{}) {
	if tmpl, ok := templateCache[viewName]; ok {
		tmpl.Execute(w, data)
		return
	}

	tmpl, err := template.New("base2.html").Funcs(templateFuncMap).ParseFiles("views/base2.html", "views/"+viewName)
	if err != nil {
		log.Fatalf("Error parsing templates %s: %v", viewName, err)
	}

	templateCache[viewName] = tmpl
	err = tmpl.Execute(w, data)
	if err != nil {
		println(err.Error())
	}
}
func adminRenderView(viewName string, w http.ResponseWriter, data interface{}) {
	if tmpl, ok := templateCache[viewName]; ok {
		tmpl.Execute(w, data)
		return
	}
	tmpl, err := template.New("admin-base.html").Funcs(templateFuncMap).ParseFiles("views/admin-base.html", "views/"+viewName)

	if err != nil {
		log.Fatal("Error parsing templates %s: %v", viewName, err)
	}
	templateCache[viewName] = tmpl
	tmpl.Execute(w, data)
}

func safeHTML(html string) template.HTML {
	return template.HTML(html)
}

//parse index html page
func indexPage(w http.ResponseWriter, r *http.Request) {

	context := GetHome(w, r)

	username := getUserName(r)
	context.Username = username

	role := QueryUser(username)
	context.Role = role.Role

	renderView("index.html", w, context)
}

func notFound(w http.ResponseWriter, r *http.Request) {
	context := Get404()
	renderView("404.html", w, context)
}

func renderAllAdminStudentsPage(w http.ResponseWriter, r *http.Request) {
	currentPath := r.URL.Path
	context := GetAdminAllStudents(w, r)
	username := getUserName(r)
	context.Username = username

	role := QueryUser(username)

	if role.Role == "pastoral" && currentPath == "/admin/students" {
		http.Error(w, "You don't have authorisation to view this page", http.StatusForbidden)
		return
	}

	if len(username) != 0 {
		adminRenderView("admin-students.html", w, context)
	}
}
func renderAddStudentPage(w http.ResponseWriter, r *http.Request) {
	context := GetInsertStudent()

	username := getUserName(r)
	context.Username = username

	if len(username) != 0 {
		adminRenderView("admin-add-student.html", w, context)
		return
	}
}
func renderAdminUersPage(w http.ResponseWriter, r *http.Request) {
	currentPath := r.URL.Path
	context := GetAdminUsers(w, r)
	username := getUserName(r)
	role := QueryUser(username)
	context.Username = username

	if role.Role == "pastoral" && currentPath == "/admin/users" {
		http.Error(w, "You don't have authorisation to view this page", http.StatusForbidden)
		return
	}
	if len(username) != 0 {
		adminRenderView("admin-users.html", w, context)
	}
}
func renderAdminHelpers(w http.ResponseWriter, r *http.Request) {
	currentPath := r.URL.Path
	context := GetAdminHelpers(w, r)
	context.ShowImpact = getAllImpacts(w, r)
	context.ShowSubject = getAllSubjects(w, r)
	context.ShowInterventions = getAllInterventions(w, r)
	context.SHowTeachers = getAllTeachers(w, r)
	username := getUserName(r)
	role := QueryUser(username)
	context.Username = username

	if role.Role == "pastoral" && currentPath == "/admin/helpers" {
		http.Error(w, "You don't have authorisation to view this page", http.StatusForbidden)
		return
	}
	if len(username) != 0 {
		adminRenderView("helpers.html", w, context)
	}
}
func renderAdminRecords(w http.ResponseWriter, r *http.Request) {
	context := EditRecords()
	context.StudentRecords = getAllStudentRecords(w, r)
	username := getUserName(r)
	if len(username) != 0 {
		adminRenderView("admin-records.html", w, context)
	}
}

func main() {

	http.Handle("/static/", http.StripPrefix("/static", http.FileServer(http.Dir("static"))))
	router := mux.NewRouter()
	router.HandleFunc("/", indexPage)
	router.HandleFunc("/get/api/all-students", GetJSONAllStudentsNames).Methods("GET")
	router.HandleFunc("/get/api/all-impacts", GetJSONAllImpacts).Methods("GET")
	router.HandleFunc("/get/api/all-subjects", GetJSONAllSubjects).Methods("GET")
	router.HandleFunc("/get/api/all-interventions", GetJSONAllInterventions).Methods("GET")
	router.HandleFunc("/get/api/all-teachers", GetJSONAllTeachers).Methods("GET")
	router.HandleFunc("/post/api/add-student-statistics", AddStudentJSONImpacts).Methods("POST")

	http.Handle("/admin-static/", AdminCheck(http.StripPrefix("/admin-static", http.FileServer(http.Dir("admin-static"))), false))
	router.Handle("/admin", AdminCheck(http.HandlerFunc(Admin), true))
	router.HandleFunc("/admin/login", login)
	router.Handle("/admin/logout", AdminCheck(http.HandlerFunc(logout), true))
	// router.HandleFunc("/admin/register", Register)

	router.Handle("/admin/add-student", AdminCheck(http.HandlerFunc(renderAddStudentPage), true))
	router.Handle("/admin/students", AdminCheck(http.HandlerFunc(renderAllAdminStudentsPage), true))
	router.Handle("/admin/helpers", AdminCheck(http.HandlerFunc(renderAdminHelpers), true))

	router.Handle("/admin/api/add-student", AdminCheck(http.HandlerFunc(InsertJSONStudent), true))
	router.Handle("/admin/api/get-single-student/{id}", AdminCheck(http.HandlerFunc(GetJSONSingleStudent), true))
	router.Handle("/admin/api/update-student/{id}", AdminCheck(http.HandlerFunc(UpdateJSONStudent), true))
	router.Handle("/admin/api/delete-student/{id}", AdminCheck(http.HandlerFunc(DeleteJSONStudent), true))

	router.Handle("/admin/users", AdminCheck(http.HandlerFunc(renderAdminUersPage), true))
	router.Handle("/admin/api/create-new-user", AdminCheck(http.HandlerFunc(InsertJSONAdminUser), true))
	router.Handle("/admin/api/get-single-user/{id}", AdminCheck(http.HandlerFunc(GetJSONSingleUser), true))
	router.Handle("/admin/api/update-single-user/{id}", AdminCheck(http.HandlerFunc(UpdateUserJSON), true))
	router.Handle("/admin/api/update-is-active-user/{id}", AdminCheck(http.HandlerFunc(EnableDisableUser), true))

	router.Handle("/admin/api/add-impact", AdminCheck(http.HandlerFunc(InsertJSONImpact), true))
	router.Handle("/admin/api/add-subject", AdminCheck(http.HandlerFunc(InsertJSONSubjects), true))
	router.Handle("/admin/api/add-intervention", AdminCheck(http.HandlerFunc(InsertJSONIntervention), true))
	router.Handle("/admin/api/add-teacher", AdminCheck(http.HandlerFunc(InsertJSONTeacher), true))

	router.Handle("/admin/api/get-single-teacher/{id}", AdminCheck(http.HandlerFunc(GetJSONSingleTeacher), true))
	router.Handle("/admin/api/update-single-teacher/{id}", AdminCheck(http.HandlerFunc(UpdateTeacherJSON), true))
	router.Handle("/admin/api/delete-teacher/{id}", AdminCheck(http.HandlerFunc(DeleteJSONTeacher), true))

	router.Handle("/admin/api/get-single-impact/{id}", AdminCheck(http.HandlerFunc(GetJSONSingleImpact), true))
	router.Handle("/admin/api/update-single-impact/{id}", AdminCheck(http.HandlerFunc(UpdateImpactJSON), true))
	router.Handle("/admin/api/delete-impact/{id}", AdminCheck(http.HandlerFunc(DeleteJSONImpact), true))

	router.Handle("/admin/api/get-single-subject/{id}", AdminCheck(http.HandlerFunc(GetJSONSingleSubject), true))
	router.Handle("/admin/api/update-single-subject/{id}", AdminCheck(http.HandlerFunc(UpdateSubjectJSON), true))
	router.Handle("/admin/api/delete-subject/{id}", AdminCheck(http.HandlerFunc(DeleteJSONSubject), true))

	router.Handle("/admin/api/get-single-intervention/{id}", AdminCheck(http.HandlerFunc(GetJSONSingleIntervention), true))
	router.Handle("/admin/api/update-single-intervention/{id}", AdminCheck(http.HandlerFunc(UpdateInterventionJSON), true))
	router.Handle("/admin/api/delete-intervention/{id}", AdminCheck(http.HandlerFunc(DeleteJSONIntervention), true))

	router.Handle("/admin/student-records", AdminCheck(http.HandlerFunc(renderAdminRecords), true))
	router.Handle("/admin/api/get-single-student-record/{id}", AdminCheck(http.HandlerFunc(GetJSONSingleStudentRecord), true))
	router.Handle("/admin/api/update-single-student-record/{id}", AdminCheck(http.HandlerFunc(UpdateStudentRecordJSON), true))
	router.Handle("/admin/api/delete-simgle-student-redord/{id}", AdminCheck(http.HandlerFunc(DeleteJSONStudentRecord), true))

	router.NotFoundHandler = http.HandlerFunc(notFound)
	http.Handle("/", router)

	err := checkDatabase()
	if err != nil {
		println(err.Error())
		createDatabase()
		return
	}
	// http.ListenAndServe(":"+os.Getenv("HTTP_PLATFORM_PORT"), nil)
	http.ListenAndServe(":3000", nil)
}

var gulp = require('gulp'),
  child = require('child_process');


gulp.task('build', function () {
  child.spawnSync('gb', ['build', 'all'], { stdio: 'inherit' });
});


var server;
gulp.task('run', function () {
  if (server) {
    server.kill();
  }
  server = child.spawn('../../bin/server', { cwd: 'src/server', stdio: 'inherit', env: { 'MYSQL_PWD': '','MYSQL_USER':'root','MYSQL_PASS':'SailIn1985!','MYSQL_HOST':'localhost:3306','MYSQL_DB':'biddulph','HTTP_PLATFORM_PORT':'3000' }});
});

gulp.task('default', ['build', 'run'], function() {
  gulp.watch(['src/**/*.go'], ['build', 'run']); // rebuild source when code changes
  gulp.watch(['src/server/views/**/*.html'], ['run']); // restart server when templates change
});
